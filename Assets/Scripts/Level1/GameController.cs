﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



namespace Level1
{
    public class GameController : MonoBehaviour
    {
        [SerializeField] private GameObject ballGO;

        #region Singleton
        private static GameController _instance;
        public static GameController Instance
        {
            get
            {
                if (_instance == null) _instance = FindObjectOfType<GameController>();
                return _instance;
            }
        }
        #endregion


        [Range (1, 4)] public int ballsCount;


        [HideInInspector] public int selectedBallNumber = 1;
        [HideInInspector] private Ball selectedBall;
        
        private PathReader pathReader;

        private List<Ball> balls;




        private void Start()
        {
            pathReader = GetComponent<PathReader>();
            pathReader.GetData();

            selectedBallNumber = 1;

            SpawnBalls();

            UIController.Instance.Initialize();

            UIController.OnSpeedChanged += ChangeBallSpeed;
        }

        private void ChangeBallSpeed(int value)
        {
            selectedBall.speed = value;
        }

        private void SpawnBalls()
        {
            balls = new List<Ball>();

            for (int i = 0; i < ballsCount; i++)
            {
                balls.Add(Instantiate(ballGO, pathReader.pathData[i].GetVector3(0), Quaternion.identity).GetComponent<Ball>());
                balls[i].Initialize(pathReader.pathData[i]);
            }

            ChangeSelectedBall();
        }

        private void ChangeSelectedBall()
        {
            if (selectedBall != null)
            {
                selectedBall.gameObject.SetActive(false);
                selectedBall.speed = 0;
            }
            UIController.Instance.ResetSpeed();

            selectedBall = balls[selectedBallNumber - 1];
            selectedBall.gameObject.SetActive(true);
        }

        //It can be another class
        #region BallSelecting 

        public bool IsNextContain()
        {
            return selectedBallNumber < ballsCount;
        }

        public bool IsPrevContain()
        {
            return selectedBallNumber > 1;
        }

        public void SelectNextBall()
        {
            if (IsNextContain())
            {
                selectedBallNumber++;
                ChangeSelectedBall();
            }
        }

        public void SelectPreviousBall()
        {
            if (IsPrevContain())
            {
                selectedBallNumber--;
                ChangeSelectedBall();
            }
        }
        #endregion
    }
}
