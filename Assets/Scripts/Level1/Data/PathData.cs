﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Level1
{
    public class PathData
    {
        public List<float> x;
        public List<float> y;
        public List<float> z;


        public int Length
        {
            get
            {
                return Math.Min(Math.Min(x.Count, y.Count), z.Count);
            }
        }

        public Vector3 GetVector3(int index)
        {
            if (index < Length)
            {
                return new Vector3(x[index], y[index], z[index]);
            }
            else
            {
                Debug.Log("You try get value out of the bounds");
                return Vector3.zero;
            }
        }
    }
}