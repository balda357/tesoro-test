﻿using UnityEngine;




namespace Level1
{
    public class PathReader : MonoBehaviour
    {

        public PathData[] pathData;


        public void GetData()
        {
            int ballsCount = GameController.Instance.ballsCount;

            pathData = new PathData[ballsCount];

            for (int i = 0; i < ballsCount; i++)
            {
                GetBallData(i);
            }


            SpawnCheckpoints();
        }

        private void GetBallData(int ballNumber)
        {
            var jsonText = Resources.Load<TextAsset>("JSON/ball_path" + (ballNumber+1));

            pathData[ballNumber] = JsonUtility.FromJson<PathData>(jsonText.ToString());
        }






        [SerializeField] private Transform CheckpointsRoot;

        [SerializeField] private GameObject[] Checkpoints;


        private void SpawnCheckpoints()
        {
            for (int i = 0; i < pathData.Length; i++)
            {
                for (int j = 0; j < pathData[i].Length; j++)
                {
                    Instantiate(Checkpoints[i], pathData[i].GetVector3(j), Quaternion.identity, CheckpointsRoot);
                }
            }
        }

    }
}