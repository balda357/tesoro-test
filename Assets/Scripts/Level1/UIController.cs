﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Level1
{
    public class UIController : MonoBehaviour
    {
        #region Singleton
        private static UIController _instance;
        public static UIController Instance
        {
            get
            {
                if (_instance == null) _instance = FindObjectOfType<UIController>();
                return _instance;
            }
        }
        #endregion


        public static Action<int> OnSpeedChanged;

        [SerializeField] private TextMeshProUGUI ballsNumText;
        [SerializeField] private Slider speedSlider;
        [SerializeField] private Button nextBallBtn;
        [SerializeField] private Button prevBallBtn;


        public void Initialize()
        {
            UpdateSelectedBall();
        }


        public void ResetSpeed()
        {
            speedSlider.value = 0;  
        }

        public void OnSpeedValueChanged()
        {
            if (OnSpeedChanged != null)
                OnSpeedChanged((int)(speedSlider.value));
        }


        public void OnNextBall()
        {
            GameController.Instance.SelectNextBall();
            UpdateSelectedBall();
        }

        public void OnPrevBall()
        {
            GameController.Instance.SelectPreviousBall();
            UpdateSelectedBall();
        }

        private void UpdateSelectedBall()
        {
            ballsNumText.text = GameController.Instance.selectedBallNumber.ToString();

            nextBallBtn.interactable = GameController.Instance.IsNextContain();
            prevBallBtn.interactable = GameController.Instance.IsPrevContain();
        }
    }
}