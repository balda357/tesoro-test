﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace Level1
{
    public class Clicker : MonoBehaviour
    {
        public Action OnClick;
        public Action OnDoubleClick;


        private bool isClicked = false;

        private float clickDelayTimer = 0;

        [SerializeField] private float clickDelayTime = .1f;

        private void OnMouseDown()
        {
            if (!isClicked)
            {
                isClicked = true;
                return;
            }

            if (isClicked && clickDelayTimer <= clickDelayTime)
                DoubleClick();
        }

        private void Update()
        {
            if (isClicked)
            {
                clickDelayTimer += Time.deltaTime;

                if (clickDelayTimer > clickDelayTime)
                    Click();
            }
        }

        private void ResetClickValues()
        {
            isClicked = false;
            clickDelayTimer = 0;
        }


        private void Click()
        {
            ResetClickValues();

            OnClick?.Invoke();
        }

        private void DoubleClick()
        {
            ResetClickValues();

            OnDoubleClick?.Invoke();
        }


    }
}
