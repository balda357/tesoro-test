﻿using DG.Tweening;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace Level1
{
    public class Ball : MonoBehaviour
    {

        [Range(0, 100)] public int speed;

        private PathData pathData;

        private int currentPointIndex = 0;

        private bool isClicked = false;

        private Clicker clicker;
        private TrailRenderer trailRenderer;



        private void Start()
        {
            trailRenderer = GetComponent<TrailRenderer>();


            clicker = GetComponentInChildren<Clicker>();

            clicker.OnDoubleClick += ToStart;
            clicker.OnClick += OnClick;
        }

        public void Initialize(PathData pd)
        {
            pathData = pd;

            currentPointIndex = 0;
        }




        private void Update()
        {
            if (!isClicked) return;

            if (currentPointIndex == pathData.Length - 1)
            {
                //speed = 0;
                UIController.Instance.ResetSpeed();
                return;
            }

            var nextPoint = pathData.GetVector3(currentPointIndex + 1);

            transform.position = Vector3.MoveTowards(transform.position, nextPoint, speed/10 * Time.deltaTime);

            if (Vector3.Distance(transform.position, nextPoint) < 0.1f)
                currentPointIndex++;

        }

        private void OnClick()
        {
            isClicked = true;
            transform.GetChild(0).DOShakeScale(.5f);
        }

        private void ToStart()
        {
            isClicked = false;
            currentPointIndex = 0;
            transform.position = pathData.GetVector3(currentPointIndex);
            transform.GetChild(0).DOShakeScale(.5f);
            trailRenderer.Clear();
        }
    }
}
